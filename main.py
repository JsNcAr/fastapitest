from fastapi import FastAPI, Body, status, HTTPException
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.staticfiles import StaticFiles
from starlette.responses import FileResponse
from config.database import engine, Base
from routers.movie import movie_router
from routers.users import users_router
from middlewares.error_handler import ErrorHandler

# Create a FastAPI instance
app = FastAPI()
app.title = "Movies API"

app.include_router(movie_router)
app.include_router(users_router)
app.add_middleware(ErrorHandler)

# Create database tables if they don't exist
Base.metadata.create_all(engine)

# Mount the "static" directory to serve static files
app.mount("/static",
          StaticFiles(directory="./therjdev.github.io/"),
          name="static")


# Home route to serve the main index.html file
@app.get("/",
         tags=["home"],
         response_class=HTMLResponse,
         status_code=status.HTTP_200_OK)
async def index(index_html: str = "index.html"):
    return FileResponse("./therjdev.github.io/" + index_html)


# Additional route to serve files from the root directory
@app.get("/{file}",
         tags=["home2"],
         response_class=HTMLResponse,
         status_code=status.HTTP_200_OK)
async def get_file(file: str):
    return FileResponse("./therjdev.github.io/" + file)


# python -m uvicorn main:app --reload --host 0.0.0.0 --port 8000