from jwt import encode, decode
import os
from dotenv import load_dotenv

load_dotenv()

JWT_SECRET = os.getenv('API_KEY')

def create_jwt_token(payload, secret=JWT_SECRET) -> str:
    return encode(payload, secret, algorithm='HS256')

def validate_jwt_token(token, secret=JWT_SECRET) -> dict:
    try:
        return decode(token, secret, algorithms=['HS256'])
    except:
        return None