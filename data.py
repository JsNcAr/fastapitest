movies = [
    {
        'id': 1,
        'title': 'Avatar',
        'overview': "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
        'year': 2009,
        'rating': 7.8,
        'category': 'Acción'
    },
    {
        'id': 2,
        'title': 'Avengers: Endgame',
        'overview': "Después de los eventos devastadores de 'Avengers: Infinity War', el universo ...",
        'year': 2019,
        'rating': 8.4,
        'category': 'Acción'
    },
    {
        'id': 3,
        'title': 'Titanic',
        'overview': "Jack (DiCaprio), un joven artista, gana en una partida de cartas un pasaje para ...",
        'year': 1997,
        'rating': 7.8,
        'category': 'Drama'
    },
]