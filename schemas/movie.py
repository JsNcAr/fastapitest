from pydantic import BaseModel, Field
from typing import List, Optional
import datetime


# Se crea clase Movie, que hereda de BaseModel
class Movie(BaseModel):
    id: Optional[int] = None  # Otra forma
    title: str
    overview: str
    year: int = Field(le=datetime.date.today().year)
    rating: float = Field(ge=0, le=10)
    category: str

    class Config:
        schema_extra = {
            "example": {
                'id': 1,
                'title': 'Name of a movie!',
                'overview': "A description of a movie!",
                'year': datetime.date.today().year,
                'rating': 10,
                'category': 'Action'
            }
        }
