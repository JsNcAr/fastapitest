from pydantic import BaseModel


class User(BaseModel):
    username: str
    password: str
    email: str
    full_name: str
    disabled: bool = None

    class Config:
        schema_extra = {
            "example": {
                "username": "johndoe",
                "password": "secret",
                "email": "mail@ghost.com",
                "full_name": "John Doe",
                "disabled": False
            }
        }


# Create a separate model for login
class UserLogin(BaseModel):
    username: str
    password: str
