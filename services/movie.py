from models.movie import Movie as MovieModel
from schemas.movie import Movie as MovieSchema
from fastapi import HTTPException, status


class MovieService:
    def __init__(self, db) -> None:
        self.db = db

    def get_all_movies(self):
        result = self.db.query(MovieModel).all()
        return result

    def get_movie_by_id(self, movie_id: int):
        return self.db.query(MovieModel).filter(
            MovieModel.id == movie_id).first()

    def get_movies_by_category(self, category: str):
        return self.db.query(MovieModel).filter(
            MovieModel.category == category).all()

    def create_movie(self, movie: MovieSchema):
        db_movie = MovieModel(**movie.dict())
        self.db.add(db_movie)
        self.db.commit()
        return db_movie

    def update_movie(self, movie_id: int, updated_movie: MovieSchema):
        result = self.db.query(MovieModel).filter(MovieModel.id == movie_id)
        if not result:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail="Movie not found")
        data = updated_movie.dict(exclude_unset=True)
        exclude_fields = ["id"]
        if all(field in list(data.keys()) for field in exclude_fields):
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                                detail="Non updatable field(s) provided")
        result.update(data)
        self.db.commit()
        return result

    def delete_movie(self, movie_id: int):
        result = self.db.query(MovieModel).filter(MovieModel.id == movie_id)
        if not result:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail="Movie not found")
        self.db.delete(result)
        self.db.commit()
        return result