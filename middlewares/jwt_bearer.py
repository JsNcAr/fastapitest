from fastapi.security import HTTPBearer
from fastapi import HTTPException, Request, status
from utils.jwt_manager import validate_jwt_token


class JWTBearer(HTTPBearer):
    # def __init__(self, auto_error: bool = True):
    #     super(JWTBearer, self).__init__(auto_error=auto_error)):

    async def __call__(self, request: Request):
        auth = await super().__call__(request)
        data: dict = validate_jwt_token(auth.credentials)
        if data.get("username") == "therjdev":
            request.state.user = data
            return auth.credentials
        else:
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                                detail="Invalid credentials")
