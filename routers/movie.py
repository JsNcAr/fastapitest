from fastapi import APIRouter, status, Depends, HTTPException
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from middlewares.jwt_bearer import JWTBearer
from config.database import Session
from models.movie import Movie as MovieModel
from typing import List, Optional
from services.movie import MovieService
from schemas.movie import Movie

movie_router = APIRouter()


# Get all movies route
@movie_router.get("/movies",
                  tags=["movies"],
                  response_model=List[Movie],
                  status_code=status.HTTP_200_OK
                  )  # dependencies=[Depends(JWTBearer())]
async def get_movies():
    db = Session()
    results = MovieService(db).get_all_movies()
    return JSONResponse(status_code=status.HTTP_200_OK,
                        content=jsonable_encoder(results))


# Get a specific movie by ID route
@movie_router.get("/movies/{movie_id}",
                  tags=["movies"],
                  status_code=status.HTTP_200_OK)
async def get_movie(movie_id: int):
    db = Session()
    result = MovieService(db).get_movie_by_id(movie_id)
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Movie not found")
    return JSONResponse(status_code=status.HTTP_200_OK,
                        content=jsonable_encoder(result))


# Get movies by category route
@movie_router.get("/movies/", tags=["movies"])
async def get_movies_by_category(category: str):
    db = Session()
    results = MovieService(db).get_movies_by_category(category)
    if not results:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Category not found")
    return JSONResponse(status_code=status.HTTP_200_OK,
                        content=jsonable_encoder(results))


# Add a new movie route
@movie_router.post("/movies",
                   tags=["movies"],
                   response_model=dict[str, str],
                   status_code=status.HTTP_201_CREATED)
async def add_movie(movie: Movie):
    db = Session()
    MovieService(db).create_movie(movie)
    message = {"message": "Movie added"}
    return message


# Edit a movie by ID route
@movie_router.put("/movies/{movie_id}",
                  tags=["movies"],
                  response_model=dict[str, str],
                  status_code=status.HTTP_200_OK)
async def update_movie(movie_id: int, updated_movie: Movie) -> dict[str, str]:
    db = Session()
    result = MovieService(db).get_movie_by_id(movie_id)
    return {"message": "Movie updated"}


# Delete a movie by ID route
@movie_router.delete("/movies/{movie_id}",
                     tags=["movies"],
                     response_model=dict[str, str],
                     status_code=status.HTTP_200_OK)
async def delete_movie(movie_id: int) -> dict[str, str]:
    db = Session()
    result = MovieService(db).delete_movie(movie_id)
    return {"message": "Movie deleted"}
