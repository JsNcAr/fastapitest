from fastapi import Body, status, APIRouter
from utils.jwt_manager import create_jwt_token
from fastapi.responses import JSONResponse
from schemas.users import UserLogin, User

users_router = APIRouter()


# Login route
@users_router.post("/login", tags=["login"], status_code=status.HTTP_200_OK)
async def login(user_login: UserLogin):
    if user_login.username == "therjdev" and user_login.password == "secret":
        token: str = create_jwt_token({
            "username": user_login.username,
            "password": user_login.password
        })
        return {"token": token}
    else:
        return JSONResponse(status_code=status.HTTP_401_UNAUTHORIZED,
                            content={"message": "Invalid credentials"})
