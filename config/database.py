import os
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

sqlite_db = os.path.join(os.path.dirname(__file__), '../db.sqlite3')

db_url = f"sqlite:///{sqlite_db}"

engine = create_engine(db_url,
                       connect_args={"check_same_thread": False},
                       echo=True)

Session = sessionmaker(bind=engine)
Base = declarative_base()